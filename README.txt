Installation

Enable module (it creates field collection 'Google Events').
Add this field collection to every content type that you want.
Note: you need Google Analitycs tracking code on your site.

Usage

On particular node in 'Google Events' field collection fill all 4 fields:
1. Target Class (item class that you want to track)
2. Event Type (use mousedown or submit)
3. Event Category (event category in GA dashboard. Example, 'test category')
4. Event Label (event label in GA dashboard. Example, 'test label')
